<?php
class Clientes extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Cliente");
	}
	public function index()
	{
		$data["listadoClientes"]=$this->Cliente->obtenerTodo();
		$this->load->view('header');
		$this->load->view('clientes/index',$data);
    $this->load->view('footer');
	}

  public function nuevo(){
    $this->load->view('header');
		$this->load->view('clientes/nuevo');
		$this->load->view('footer');
  }
  public function bienvenido(){
    $this->load->view('header');
		$this->load->view('clientes/bienvenido');
		$this->load->view('footer');
  }
  public function nosotros(){
    $this->load->view('header');
    $this->load->view('clientes/nosotros');
    $this->load->view('footer');
  }

	public function guardarClientes(){
		$datosNuevo=array(
      "nombre_clie"=>$this->input->post("nombre_clie"),
      "apellido_clie"=>$this->input->post("apellido_clie"),
      "telefono_clie"=>$this->input->post("telefono_clie"),
      "email_clie"=>$this->input->post("email_clie"),
      "latitud_clie"=>$this->input->post("latitud_clie"),
      "longitud_clie"=>$this->input->post("longitud_clie"),
		);
		print_r($datosNuevo);
		if ($this->Cliente->insertar($datosNuevo)) {
      echo "<h1>informacion ingresada</h1>";
		}else{
      echo "<h1>Error al insertar</h1>";
		}
		redirect('clientes/index');
		}

	   public function borrar($id_clie){
		     if ($this->Cliente->eliminarPorId($id_clie)) {

		} else {

		}
		redirect('clientes/index');
	}

}//no cerrar
