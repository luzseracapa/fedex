<?php
class Encomiendas extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Encomienda");
	}
	public function index()
	{
		$data["listadoEncomiendas"]=$this->Encomienda->obtenerTodo();
		$this->load->view('header');
		$this->load->view('encomiendas/index',$data);
    $this->load->view('footer');
	}

  public function nuevo(){
    $this->load->view('header');
		$this->load->view('encomiendas/nuevo');
		$this->load->view('footer');
  }
	public function guardarEncomiendas(){
    $datosNuevo = array(
        "nombre_clie_enco" => $this->input->post("nombre_clie_enco"),
        "tipo_envio_enco" => $this->input->post("tipo_envio_enco"),
        "latitud_clie_enco" => $this->input->post("latitud_clie_enco"),
        "longitud_clie_enco" => $this->input->post("longitud_clie_enco"),
        "pedido_id_ped" => $this->input->post("pedido_id_ped"),
    );
		print_r($datosNuevo);
    if ($this->Encomienda->insertar($datosNuevo)) {
        echo "<h1>informacion ingresada</h1>";
    } else {
        echo "<h1>Error al insertar</h1>";
    }

    redirect('encomiendas/index');
}
	   public function borrar($id_enco){
		     if ($this->Encomienda->eliminarPorId($id_enco)) {

		} else {

		}
		redirect('encomiendas/index');
	}

}//no cerrar
