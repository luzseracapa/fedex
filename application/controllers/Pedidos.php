<?php
class Pedidos extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Pedido");
	}
	public function index()
	{
		$data["listadoPedidos"]=$this->Pedido->obtenerTodo();
		$this->load->view('header');
		$this->load->view('pedidos/index',$data);
    $this->load->view('footer');
	}

  public function nuevo(){
    $this->load->view('header');
		$this->load->view('pedidos/nuevo');
		$this->load->view('footer');
  }
	public function guardarPedidos(){
		$datosNuevo=array(
      "fecha_ped"=>$this->input->post("fecha_ped"),
      "descripcion_ped"=>$this->input->post("descripcion_ped"),
      "cliente_id_clie"=>$this->input->post("cliente_id_clie"),
      "latitud_ped"=>$this->input->post("latitud_ped"),
      "longitud_ped"=>$this->input->post("longitud_ped"),
			"latitud_envio"=>$this->input->post("latitud_envio"),
			"longitud_envio"=>$this->input->post("longitud_envio"),
		);
		print_r($datosNuevo);
		if ($this->Pedido->insertar($datosNuevo)) {
      echo "<h1>informacion ingresada</h1>";
		}else{
      echo "<h1>Error al insertar</h1>";
		}
		redirect('pedidos/index');
		}

	   public function borrar($id_ped){
		     if ($this->Pedido->eliminarPorId($id_ped)) {

		} else {

		}
		redirect('pedidos/index');
	}

}//no cerrar
