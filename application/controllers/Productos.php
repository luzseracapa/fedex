<?php
    class Productos extends CI_Controller
    {
        function __construct()
        {
            parent:: __construct();
            //cargar modelo
            $this->load->model('Producto');
        }

        public function index()
        {
            $data["listadoProductos"]=$this->Producto->obtenerTodos();
            $this->load->view('header');
            $this->load->view('productos/index',$data);
            $this->load->view('footer');
        }
        //fucion que renderiza la vista nuevo
        public function nuevo(){
            $this->load->view('header');
            $this->load->view('productos/nuevo');
            $this->load->view('footer');
        }

        // nuevo insercion
        public function guardarProductos(){
            $datosNuevoProducto=array(
                "nombre_pro"=>$this->input->post('nombre_pro'),
                "descripcion_pro"=>$this->input->post('descripcion_pro'),
                "precio_pro"=>$this->input->post('precio_pro'),
                "pedido_id_ped"=>$this->input->post('pedido_id_ped'),
            );
            print_r($datosNuevoProducto);
        		if ($this->Producto->insertar($datosNuevoProducto)) {
              echo "<h1>informacion ingresada</h1>";
        		}else{
              echo "<h1>Error al insertar</h1>";
        		}
        		redirect('productos/index');
        		}

        	   public function borrar($id_pro){
        		     if ($this->Producto->eliminarPorId($id_pro)) {

        		} else {

        		}
        		redirect('productos/index');
        	}



    }////cierre de clase

?>
