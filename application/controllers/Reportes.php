<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {
	//agregar el constructor para iniciar la clase
	function __construct()
	{
		parent::__construct();
		$this->load->model("Reporte"); //llamar al modelo con el mismo nombre
	}

	public function clientes(){
			$data["cliente"]=$this->Reporte->obtenerClientes();
			$this->load->view('header');
			$this->load->view('reportes/clientes',$data);
			$this->load->view('footer');
		}
    public function pedidos(){
  			$data["pedido"]=$this->Reporte->obtenerPedidos();
  			$this->load->view('header');
  			$this->load->view('reportes/pedidos',$data);
  			$this->load->view('footer');
  		}
			public function sucursales(){
					$data["sucursal"]=$this->Reporte->obtenerSucursales();
					$this->load->view('header');
					$this->load->view('reportes/sucursales',$data);
					$this->load->view('footer');
				}
				public function encomiendas(){
		$data["encomienda"] = $this->Reporte->obtenerEncomiendas();
		$this->load->view('header');
		$this->load->view('reportes/encomiendas', $data);
		$this->load->view('footer');
	}

  }
