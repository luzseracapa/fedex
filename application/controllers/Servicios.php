<?php
class Servicios extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		// $this->load->model("Cliente");
	}
  public function aero(){
    $this->load->view('header');
		$this->load->view('servicios/aero');
		$this->load->view('footer');
  }
  public function maritimo(){
    $this->load->view('header');
		$this->load->view('servicios/maritimo');
		$this->load->view('footer');
  }
  public function complementarios(){
    $this->load->view('header');
    $this->load->view('servicios/complementarios');
    $this->load->view('footer');
  }
	//servicio de contactos
	public function contactanos(){
    $this->load->view('header');
    $this->load->view('servicios/contactanos');
    $this->load->view('footer');
  }

}//no cerrar
