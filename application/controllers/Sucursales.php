<?php
class Sucursales extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Sucursal");
	}
	public function index()
	{
		$data["listadoSucursales"]=$this->Sucursal->obtenerTodo();
		$this->load->view('header');
		$this->load->view('sucursales/index',$data);
    $this->load->view('footer');
	}

  public function nuevo(){
    $this->load->view('header');
		$this->load->view('sucursales/nuevo');
		$this->load->view('footer');
  }
	public function guardarSucursales(){
		$datosNuevo=array(
      "nombre_suc"=>$this->input->post("nombre_suc"),
      "telefono_suc"=>$this->input->post("telefono_suc"),
      "email_suc"=>$this->input->post("email_suc"),
      "latitud_suc"=>$this->input->post("latitud_suc"),
      "longitud_suc"=>$this->input->post("longitud_suc"),
		);
		print_r($datosNuevo);
		if ($this->Sucursal->insertar($datosNuevo)) {
      echo "<h1>informacion ingresada</h1>";
		}else{
      echo "<h1>Error al insertar</h1>";
		}
		redirect('sucursales/index');
		}

	   public function borrar($id_suc){
		     if ($this->Sucursal->eliminarPorId($id_suc)) {

		} else {

		}
		redirect('sucursales/index');
	}

}//no cerrar
