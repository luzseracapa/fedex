<?php
  class Cliente extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("cliente",$datos);
    }
    public function obtenerTodo(){
      $clientes=$this->db->get('cliente');
      if ($clientes->num_rows()>0) {
        return $clientes->result();
      } else {
        return false;
      }
    }
    public function eliminarPorId($id){
      $this->db->where("id_clie",$id);
      return $this->db->delete('cliente');
    }
}//cierre llave
