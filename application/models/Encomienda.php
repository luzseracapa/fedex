<?php
  class Encomienda extends CI_Model
  {
    function __construct()
    {
    		parent:: __construct();

    }

    public function insertar($datos){
      return $this->db->insert("encomienda",$datos);
    }
    public function obtenerTodo(){
      $encomiendas=$this->db->get('encomienda');
      if ($encomiendas->num_rows()>0) {
        return $encomiendas->result();
      } else {
        return false;
      }
    }
    public function eliminarPorId($id){
      $this->db->where("id_enco",$id);
      return $this->db->delete('encomienda');
  }
}//cierre llave
