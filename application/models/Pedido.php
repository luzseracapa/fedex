<?php
  class Pedido extends CI_Model
  {
    function __construct()
    {
    		parent:: __construct();

    }

    public function insertar($datos){
      return $this->db->insert("pedido",$datos);
    }
    public function obtenerTodo(){
      $pedidos=$this->db->get('pedido');
      if ($pedidos->num_rows()>0) {
        return $pedidos->result();
      } else {
        return false;
      }
    }
    public function eliminarPorId($id){
      $this->db->where("id_ped",$id);
      return $this->db->delete('pedido');
  }
}//cierre llave
