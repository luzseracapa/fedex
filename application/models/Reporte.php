<?php
class Reporte extends CI_Model /// nombre del modelo tan cual esta creado en el modelo
{

  function __construct()
  {
    parent::__construct();
  }
  function obtenerClientes(){
    // $this->db->where('id_clie');
    $listadoClientes=$this->db->get("cliente");
    if ($listadoClientes->num_rows()>0){
      return $listadoClientes->result();
    }
    return false;
  }
  function obtenerPedidos(){
    // $this->db->where('id_clie');
    $listadoPedidos=$this->db->get("pedido");
    if ($listadoPedidos->num_rows()>0){
      return $listadoPedidos->result();
    }
    return false;
  }
  function obtenerSucursales(){
    // $this->db->where('id_clie');
    $listadoSucursales=$this->db->get("sucursal");
    if ($listadoSucursales->num_rows()>0){
      return $listadoSucursales->result();
    }
    return false;
  }
  function obtenerEncomiendas(){
    // $this->db->where('id_clie');
    $listadoEncomiendas=$this->db->get("encomienda");
    if ($listadoEncomiendas->num_rows()>0){
      return $listadoEncomiendas->result();
    }
    return false;
  }
}
?>
