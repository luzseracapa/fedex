<?php
  class Sucursal extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("sucursal",$datos);
    }
    public function obtenerTodo(){
      $sucursales=$this->db->get('sucursal');
      if ($sucursales->num_rows()>0) {
        return $sucursales->result();
      } else {
        return false;
      }
    }
    public function eliminarPorId($id){
      $this->db->where("id_suc",$id);
      return $this->db->delete('sucursal');
    }
}//cierre llave
