<div class="container-fluid px-0 mb-5">
    <div id="header-carousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="w-100" src="https://m.media-amazon.com/images/I/715JRiIEFSS.jpg" alt="Image">
                <div class="carousel-caption">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 text-start">
                                <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                    Trabajando contigo con experiencia</p>
                                <h1 class="display-1 text-white mb-5 animated slideInRight">
                                    Tu Company Fedex</h1>
                                <a href="" class="btn btn-primary py-3 px-5 animated slideInRight">Visitanos sera un gusto atenderle</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="w-100" src="https://newsismybusiness.com/wp-content/uploads/2022/12/FedEx-Peak-Season-1.jpg" alt="Image">
                <div class="carousel-caption">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 text-start">
                                <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                    Trabajando contigo con experiencia</p>
                                <h1 class="display-1 text-white mb-5 animated slideInRight">
                                    Tu Company Fedex</h1>
                                <a href="<?php echo site_url("clientes/nosotros"); ?>" class="btn btn-primary py-3 px-5 animated slideInRight">Visitamos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="w-100" src="https://wtop.com/wp-content/uploads/2016/12/121516_fedex_2.jpg" alt="Image">
                <div class="carousel-caption">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 text-start">
                                <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                    Trabajando contigo con experiencia</p>
                                <h1 class="display-1 text-white mb-5 animated slideInRight">
                                    Tu Company Fedex</h1>
                                <a href="" class="btn btn-primary py-3 px-5 animated slideInRight">Visitamos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#header-carousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;"> Los mejores servicios de entrega</p> </b></h1></div>
    </div>
</div>
<div class="row">
  <div class="col-md-4 text-center">
    <img src="https://www.fedex.com/content/dam/fedex/lac-latin-america/MVP/images/2020/Q1/CO_EN_2019_7_web_jpg_NA_FedEx_Post_CCA_home_138228027.jpg" alt=""height="250" ,width="250" , rotate="360°">
  </div>
  <div class="col-md-4 text-center">
    <img src="https://www.mascontainer.com/wp-content/uploads/2021/01/Fedex.jpg" alt=""height="250" ,width="250" , rotate="360°">
  </div>
  <div class="col-md-4 text-center">
    <img src="https://www.remessaonline.com.br/blog/wp-content/uploads/2022/03/como-funciona-fedex-no-brasil.jpg.optimal.jpg" alt=""height="250" ,width="250" , rotate="360°">
  </div>
</div>
