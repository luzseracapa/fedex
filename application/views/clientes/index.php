<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Lista de clientes </p> </b></h1></div>
    </div>
    <br>
    <br>
    <br>
  <?php if ($listadoClientes): ?>
  <table id="tabla-clientes" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">APELLIDO</th>
      <th class="text-center">TELEFONO</th>
      <th class="text-center">CORREO ELECTRONICO</th>
      <th class="text-center">LATITUD</th>
      <th class="text-center">LOGITUD</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoClientes as $Temporal): ?>
      <tr>
        <td class="text-center"><?php echo  $Temporal->id_clie ?></td>
        <td class="text-center"> <?php echo $Temporal->nombre_clie ?></td>
        <td class="text-center"> <?php echo $Temporal->apellido_clie?></td>
        <td class="text-center"> <?php echo $Temporal->telefono_clie ?></td>
        <td class="text-center"> <?php echo $Temporal->email_clie ?></td>
        <td class="text-center"> <?php echo $Temporal->latitud_clie?></td>
        <td class="text-center"> <?php echo $Temporal->longitud_clie?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("clientes/borrar");?>/<?php echo $Temporal->id_clie ; ?>"class="btn btn-danger">
          <i class="glyphicon glyphicon-trash" ></i>Eliminar</a>
          </button>
           <a href="<?php echo site_url("clientes/nuevo") ?>" class="btn btn-danger"></i>Agregar</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existe clientes</b></h3>
<?php endif; ?>

  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
