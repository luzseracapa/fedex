<br>
<?php if ($empleados):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1>Listado de clientes</h1>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>TELEFONO</th>
                <th>CORREO ELECTRONICO</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($clientes as $filaTemporal): ?>
            <tr>
                    <td>
                        <?php echo $filaTemporal->id_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->apellido_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->telefono_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->email_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->latitud_clie;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->longitud_clie; ?>
                    </td>
            </tr>
            <?php endforeach; ?>

        </tbody>

    </table>

    <?php else: ?>
        <h1>no hay clientes</h1>
        <?php endif;?>
