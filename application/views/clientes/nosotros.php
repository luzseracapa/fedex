<div class="container-fluid px-0 mb-5">
    <div id="header-carousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="w-100" src="https://m.media-amazon.com/images/I/715JRiIEFSS.jpg" alt="Image">
                <div class="carousel-caption">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 text-start">
                                <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                    Trabajando contigo con experiencia</p>
                                <h1 class="display-1 text-white mb-5 animated slideInRight">
                                    Tu Company Fedex</h1>
                                <a href="" class="btn btn-primary py-3 px-5 animated slideInRight">Visitanos sera un gusto atenderle</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="w-100" src="https://newsismybusiness.com/wp-content/uploads/2022/12/FedEx-Peak-Season-1.jpg" alt="Image">
                <div class="carousel-caption">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 text-start">
                                <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                    Trabajando contigo con experiencia</p>
                                <h1 class="display-1 text-white mb-5 animated slideInRight">
                                    Tu Company Fedex</h1>
                                <a href="<?php echo site_url("clientes/nosotros"); ?>" class="btn btn-primary py-3 px-5 animated slideInRight">Visitamos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="w-100" src="https://wtop.com/wp-content/uploads/2016/12/121516_fedex_2.jpg" alt="Image">
                <div class="carousel-caption">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 text-start">
                                <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                    Trabajando contigo con experiencia</p>
                                <h1 class="display-1 text-white mb-5 animated slideInRight">
                                    Tu Company Fedex</h1>
                                <a href="" class="btn btn-primary py-3 px-5 animated slideInRight">Visitamos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#header-carousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;"> Sobre nosotros</p> </b></h1></div>
    </div>
</div>
<div class="container">
  <div class="col-md-12 text-center">
    <p><b>FedEx es una empresa de servicios de mensajería y logística con sede en Estados Unidos. Fue fundada en 1971 y es una de las compañías de envío más grandes y reconocidas a nivel mundial. El nombre "FedEx" es la abreviatura de Federal Express, el nombre original de la empresa.

FedEx proporciona una amplia gama de servicios, incluyendo envío exprés, logística, servicios de paquetería y soluciones de comercio electrónico. La empresa se especializa en la entrega rápida y confiable de paquetes y documentos a nivel nacional e internacional. Utiliza una red extensa de aviones, camiones y centros de distribución para gestionar la recolección, clasificación y entrega de envíos en todo el mundo.

La empresa se ha destacado por su enfoque en la tecnología y la innovación. Fue una de las primeras en introducir el código de barras en el seguimiento de paquetes y ha implementado sistemas avanzados de logística y gestión de cadena de suministro. Además, FedEx ha establecido una fuerte presencia global con operaciones en más de 220 países y territorios.</b></p>
  </div>
</div>
<div class="container">
  <div class="col-md-12 text-center">
<style>

    table  {
      width: 100%;
      border-collapse: collapse;
    }
    th, td {
      border: 0px solid black;
      padding: 8px;
    }
    .left-column {
      width: 50%;
      font-family: 'Roboto', sans-serif;

    }
    .right-column {
      width: 50%;
        font-family: 'Roboto', sans-serif;
    }

  </style>
</head>
<body>
  <table>
    <tr>
      <th class="left-column">MISIÓN</th>
      <th class="right-column">VISIÓN</th>
    </tr>
    <tr>
      <td class="left-column"> <strong> "La misión de FedEx es ser la empresa de transporte y logística líder a nivel mundial. Nos esforzamos por ofrecer a nuestros clientes soluciones de transporte integrales, innovadoras y de alta calidad, adaptadas a sus necesidades individuales. Nos comprometemos a brindar un servicio excepcional y a mantener los más altos estándares éticos y profesionales. Al hacerlo, creamos valor para nuestros accionistas y oportunidades de crecimiento para nuestros empleados".</td>
      <td class="right-column"><strong>"La misión de FedEx es ser la empresa de transporte y logística líder a nivel mundial. Nos esforzamos por ofrecer a nuestros clientes soluciones de transporte integrales, innovadoras y de alta calidad, adaptadas a sus necesidades individuales. Nos comprometemos a brindar un servicio excepcional y a mantener los más altos estándares éticos y profesionales. Al hacerlo, creamos valor para nuestros accionistas y oportunidades de crecimiento para nuestros empleados".</td>
    </tr>
  </table>
  </div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-12 text-center">
    <img src="https://fedex-dims.brightspotgocdn.com/dims4/default/2c961f7/2147483647/strip/true/crop/799x450+0+42/resize/1000x563!/quality/90/?url=https%3A%2F%2Ffedex-static.brightspotgocdn.com%2F0a%2Fa8%2F82dcb2bbfdb47a804c2e51eaf06f%2Flogo-corporate.jpg" alt="">

  </div>

</div>
