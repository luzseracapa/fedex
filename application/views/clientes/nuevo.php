<div class="row">
    <div class="col-md-12 text-center">
        <h1><b><p style="color:#001f36;">Ingrese nuevo cliente </p></b></h1>
    </div>
</div>
<br>
<br>
<form class="" id="frm_nuevo_cliente" action="<?php echo site_url("clientes/guardarClientes"); ?>" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <label for="">Ingrese Nombre
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="text" placeholder="Ingrese su nombre" class="form-control" name="nombre_clie" value="" id="nombre_clie" required>
                <br>
            </div>
            <div class="col-md-6">
                <label for="">Ingrese su apellido
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="text" placeholder="Ingrese su apellido" class="form-control" name="apellido_clie" value="" id="apellido_clie" required>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="">Ingrese su telefono
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="number" placeholder="Ingrese su numero de telefono" class="form-control" name="telefono_clie" value="" id="telefono_clie" required>
                <br>
            </div>
            <div class="col-md-6">
                <label for="">Ingrese su correo electrónico
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="text" placeholder="Ingrese su correo electrónico" class="form-control" name="email_clie" value="" id="email_clie" required>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Ingrese su direccion</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="">Ingrese su latitud
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="float" placeholder="Ingrese su latitud" class="form-control" name="latitud_clie" value="" id="latitud_clie" readonly>
                <br>
            </div>
            <div class="col-md-3">
                <label for="">Ingrese su longitud
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="float" placeholder="Ingrese su longitud" class="form-control" name="longitud_clie" value="" id="longitud_clie" readonly>
                <br>
            </div>
            <div class="col-md-6">
                <div id="mapaUbicacion" style="height:200px; width: 100%; border:2px solid black;"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    function initMap(){
      var centro=new google.maps.LatLng(-1.642666457056745, -78.67639361548689);
      var mapa1=new google.maps.Map(
        document.getElementById('mapaUbicacion'),
        {
          center:centro,
          zoom:7,
          mapTypeId:google.maps.MapTypeId.ROADMAP
        }
      );
      var marcador=new google.maps.Marker({
        position:centro,
        map:mapa1,
        title:"Seleccione la direccion",
        icon: "<?php echo base_url('assets/images/verde.png'); ?>",
        draggable:true
      });
      ///// valores
      google.maps.event.addListener(marcador,'dragend',function(){
        // alert ("Se termino el Drag");
        document.getElementById('latitud_clie').value=
        this.getPosition().lat();
        document.getElementById('longitud_clie').value=
        this.getPosition().lng();
      });
    }///cierre de la funcion
    </script>
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <br>
            <button type="submit" name="button" class="btn btn-primary">Guardar</button>
            <a href="<?php echo site_url(); ?>/clientes/listas" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</form>
<script type="text/javascript">
    $("#frm_nuevo_cliente").validate({
        rules: {
            nombre_clie: {
                required: true,
                minlength: 3,
                maxlength: 250,
                letras: true
            },
            apellido_clie: {
                required: true,
                minlength: 3,
                maxlength: 250,
                letras: true
            },
            telefono_clie: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true
            },
        },
        messages: {
            nombre_clie: {
                required: "Ingrese su nombre por favor",
                minlength: "Ingrese el nombre al menos de 3 caracteres",
                maxlength: "Nombre incorrecto",
                letras: "Este campo acepta solo letras"
            },
            apellido_clie: {
                required: "Ingrese su apellido por favor",
                minlength: "Ingrese el apellido al menos de 3 caracteres",
                maxlength: "Apellido incorrecto",
                letras: "Este campo acepta solo letras"
            },
            telefono_clie: {
                required: "Ingrese su teléfono por favor",
                minlength: "Ingrese el teléfono al menos de 10 dígitos",
                maxlength: "Teléfono incorrecto",
                digits: "Este campo acepta solo números"
            },
        }
    });
</script>
