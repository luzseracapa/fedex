<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">LISTA DE ENCOMIENDA</p> </b></h1>
    </div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('encomiendas/nuevo'); ?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Encomienda
       </a>
    <br>
    <br>
    <br>
  <?php if ($listadoEncomiendas): ?>
  <table id="tabla-encomiendas" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">NOMBRE DE CLIENTE ENCOMIENDA</th>
      <th class="text-center">TIPO DE ENVIO</th>
      <th class="text-center">LATITUD</th>
      <th class="text-center">LONGITUD</th>
      <th class="text-center">PEDIDO</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoEncomiendas as $Temporal): ?>
      <tr>
        <td class="text-center"><?php echo  $Temporal->id_enco ?></td>
        <td class="text-center"> <?php echo $Temporal->nombre_clie_enco ?></td>
        <td class="text-center"> <?php echo $Temporal->tipo_envio_enco ?></td>
        <td class="text-center"> <?php echo $Temporal->latitud_clie_enco ?></td>
        <td class="text-center"> <?php echo $Temporal->longitud_clie_enco?></td>
        <td class="text-center"> <?php echo $Temporal->pedido_id_ped?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("encomiendas/borrar");?>/<?php echo $Temporal->id_enco ; ?>"class="btn btn-danger"
            title="Eliminar Encomienda" onclick="return confirm('Esta seguro de eliminar encomienda');">
          <i class="glyphicon glyphicon-trash" ></i>Eliminar</a>
          </button>
           <a href="<?php echo site_url("encomiendas/nuevo") ?>" class="btn btn-danger"></i>Agregar</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existen encomiendas</b></h3>
<?php endif; ?>

  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
