<?php if ($encomiendas): ?>
    <h1>Listado de encomiendas</h1>
    <table class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE CLIENTE ENCOMIENDA</th>
                <th>TIPO ENVIO</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>PEDIDO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($encomiendas as $filaTemporal): ?>
            <tr>
                <td>
                    <?php echo $filaTemporal->id_enco; ?>
                </td>
                <td>
                    <?php echo $filaTemporal->nombre_clie_enco; ?>
                </td>
                <td>
                    <?php echo $filaTemporal->tipo_envio_enco; ?>
                </td>
                <td>
                    <?php echo $filaTemporal->latitud_clie_enco; ?>
                </td>
                <td>
                    <?php echo $filaTemporal->longitud_clie_enco; ?>
                </td>
                <td>
                    <?php echo $filaTemporal->pedido_id_ped;?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <h1>No hay encomiendas</h1>
<?php endif; ?>
