<div class="row">
    <div class="col-md-12 text-center">
        <h1><b><p style="color:#001f36;">Ingrese nueva Encomienda</p></b></h1>
    </div>
</div>

<br><br>

<form class="" id="frm_nuevo_encomienda" action="<?php echo site_url("encomiendas/guardarEncomiendas"); ?>" method="post">
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <label for="">Ingrese Nombre del Cliente de encomienda
              <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese el nombre de encomienda" class="form-control" name="nombre_clie_enco" value="" id="nombre_clie_enco" required>
            <br>
        </div>
        <div class="col-md-4">
            <label for="">Ingrese el tipo de encomienda
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="text" placeholder="Ingrese el tipo de encomienda" class="form-control" name="tipo_envio_enco" value="" id="tipo_envio_enco" required>
            <br>
        </div>
        <div class="col-md-4">
            <label for="">Ingrese el id de pedido</label>
            <br>
            <input type="number" placeholder="Ingrese el id de pedido" class="form-control" name="pedido_id_ped" value="" id="pedido_id_ped" required>
            <br>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Ingrese su direccion</h3>
            </div>
        </div>
        <div class="row">
        <div class="col-md-3">
            <label for="">Ingrese la latitud de encomienda
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="number" placeholder="Ingrese la latitud encomienda" class="form-control" name="latitud_clie_enco" value="" id="latitud_clie_enco" readonly>
            <br>
        </div>
          <div class="col-md-3">
              <label for="">Ingrese su longitud encomienda
              <span class="obligatorio">(Obligatorio)</span>
            </label>
              <br>
              <input type="number" placeholder="Ingrese su longitud encomienda" class="form-control" name="longitud_clie_enco" value="" id="longitud_clie_enco" readonly>
              <br>
          </div>
          <div class="col-md-6">
              <div id="mapaUbicacion" style="height:200px; width: 100%; border:2px solid black;"></div>
          </div>
         </div>
      </div>
</div>

    <div class="row">
        <div class="col-md-12 text-center">
            <br><br>
            <button type="submit" name="button" class="btn btn-primary">Guardar</button>
            <a href="<?php echo site_url(); ?>/encomiendas/listas" class="btn btn-danger">
                Cancelar
            </a>
        </div>
    </div>

    <br>
    <br>
    <!-- //////MAPA PARA SELECCIONAR -->
    <!-- <div class="container">
        <div class="container">
            <div id="mapaUbicacion" style="height:400px; width: 100%; border:2px solid black;"></div>
        </div>
    </div> -->

    <script type="text/javascript">
        function initMap() {
            var centro = new google.maps.LatLng(-1.642666457056745, -78.67639361548689);
            var mapa1 = new google.maps.Map(document.getElementById('mapaUbicacion'), {
                center: centro,
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marcador = new google.maps.Marker({
                position: centro,
                map: mapa1,
                title: "Seleccione la direccion",
                icon: "<?php echo base_url('assets/images/verde.png'); ?>",
                draggable: true
            });
            ///// valores
            google.maps.event.addListener(marcador, 'dragend', function() {
                // alert ("Se termino el Drag");
                document.getElementById('latitud_clie_enco').value = this.getPosition().lat();
                document.getElementById('longitud_clie_enco').value = this.getPosition().lng();
            });
        }
    </script>

    <br><br>
</form>

<script type="text/javascript">
  $("#frm_nuevo_encomienda").validate({
    rules:{
      nombre_clie_enco:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      tipo_envio_enco:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      pedido_id_ped:{
        required:true,
        minlength:1,
        maxlength:250,
        digits:true
      },

    },
    messages:{
      nombre_clie_enco:{
        required:"Ingrese su nombre por favor",
        minlength:"Ingrese el nombre al menos de 3 digitos",
        maxlength:"Nombre incorrecto",
        letras:"Este campo acepta solo letras"
      },
      tipo_envio_enco:{
        required:"Ingrese su tipo por favor",
        minlength:"Ingrese el tipo al menos de 3",
        maxlength:"Tipo incorrecto",
        letras:"Este campo acepta solo letras"
      },
      pedido_id_ped:{
        required:"Ingrese su id por favor",
        minlength:"Ingrese el id al menos de 1",
        maxlength:"Incorrecto",
        digits:"Este campo acepta solo numeros"
      },


    }
  });
</script>
<br><br>
