<style media="screen">
  .obligatorio{
    color:#800000;
    background-color:white;
    border: radius 20px;
    font-size:10px;
    padding-left:6px;
    padding-right:6px;
  }
  .error{
    color:#800000;
    font-weight:bold;
  }
  input.error{
    border:2px solid red;
  }
</style>
<!-- Footer Start -->
    <div class="container-fluid bg-dark footer mt-5 py-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Nuestras oficinas</h5>
                    <p class="mb-2"><i class="fa fa-map-marker-alt me-3"></i>Av Cevallos 01-161 Y  <br> Abdon Ca 170307 Ambato
EC</p>
                    <p class="mb-2"><i class="fa fa-phone-alt me-3"></i>Cerrado Abre a las 09:00 Lunes</p>
                    <p class="mb-2"><i class="fa fa-envelope me-3"></i>fedexambato@gmail.com</p>
                    <div class="d-flex pt-3">
                        <a class="btn btn-square btn-primary rounded-circle me-2" href="https://twitter.com/fedexteayuda"><i
                                class="fab fa-twitter"></i></a>
                        <a class="btn btn-square btn-primary rounded-circle me-2" href="https://www.facebook.com/FedEx/?locale=es_LA"><i
                                class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-square btn-primary rounded-circle me-2" href="https://www.youtube.com/watch?v=H8Mj3CUsPJ8"><i
                                class="fab fa-youtube"></i></a>
                        <a class="btn btn-square btn-primary rounded-circle me-2" href="https://www.linkedin.com/company/fedex?original_referer=https%3A%2F%2Fwww.google.com%2F"><i
                                class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Enlaces</h5>
                    <a class="btn btn-link" href="<?php echo site_url("clientes/nosotros"); ?>">Sobre nosotros</a>
                    <a class="btn btn-link" href="">Contactanos</a>
                    <a class="btn btn-link" href="">Otro Servicios</a>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Horario de trabajo</h5>
                    <p class="mb-1">Lunes - Viernes</p>
                    <h6 class="text-light">09:00 am - 07:00 pm</h6>
                    <p class="mb-1">Sábado</p>
                    <h6 class="text-light">09:00 am - 12:00 pm</h6>
                    <p class="mb-1">Domingo</p>
                    <h6 class="text-light">Cerrado</h6>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Sucursales Ecuador</h5>
                    <p>Calle Chirimoyas 01-14 Y Av G 170307 Ambato EC</p>
                    <p>Av Amazonas N24-01 Y Wilson 170307 Quito EC</p>
                    <p>Av. Gil Ramírez Dávalos 1-156 y El Pedregal CC CENCOMAY 010102 Cuenca EC</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->


    <!-- Copyright Start -->
    <div class="container-fluid copyright bg-dark py-4">
        <div class="container text-center">
            <p class="mb-2">Copyright &copy; <a class="fw-semi-bold" href="#">Karo & Angelitha</a>, Todos los derechos reservados
            </p>
            <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
            <p class="mb-0">Diseñado por <a class="fw-semi-bold" href="">Las chicas dulces</a> Universidad Tecnica de Cotopaxi
                <a href="">UTC</a> </p>
        </div>
    </div>
    <!-- Copyright End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded-circle back-to-top"><i
            class="bi bi-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url('plantilla/lib/wow/wow.min.js') ?>"></script>
    <script src="<?php echo base_url('plantilla/lib/easing/easing.min.js') ?>"></script>
    <script src="<?php echo base_url('plantilla/lib/waypoints/waypoints.min.js') ?>"></script>
    <script src="<?php echo base_url('plantilla/lib/owlcarousel/owl.carousel.min.js') ?>"></script>
    <script src="<?php echo base_url('plantilla/lib/counterup/counterup.min.js') ?>"></script>

    <!-- Template Javascript -->
    <script src=<?php echo base_url('plantilla/js/main.js'); ?>></script>
</body>

</html>
