
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>FEDEX</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600&family=Rubik:wght@500;600;700&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url('plantilla/lib/animate/animate.min.css'); ?>" >
    <link rel="stylesheet" href="<?php echo base_url('plantilla/lib/owlcarousel/assets/owl.carousel.min.css'); ?>" >

    <!-- Customized Bootstrap Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url('plantilla/css/bootstrap.min.css'); ?>" >

    <!-- Template Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url('plantilla/css/style.css'); ?>" ->
    <!-- COLOCACION DE APIKEY DE GOOGLE MAPS -->
     <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ9im5myj2rT_iJPbbftxHmOdJWM9PV3g&libraries=places&callback=initMap">
     </script>
     <!-- importacion de jquery -->
    <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
    <!-- importacion de jquery validate para validacion-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     <!-- validar solo los campos en letras -->
 <script type="text/javascript">
    jQuery.validator.addMethod("letras", function(value, element) {
        //return this.optional(element) || /^[a-z]+$/i.test(value);
        return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);

		}, "Este campo solo acepta letras");
 </script>
</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner"
        class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-border text-primary" role="status" style="width: 3rem; height: 3rem;"></div>
    </div>
    <!-- Spinner End -->


    <!-- Topbar Start -->
    <div class="container-fluid bg-dark px-0">
        <div class="row g-0 d-none d-lg-flex">
            <div class="col-lg-6 ps-5 text-start">
                <div class="h-100 d-inline-flex align-items-center text-white">
                    <span>REDES SOCIALES:</span>
                    <a class="btn btn-link text-light" href="https://www.facebook.com/FedEx/?locale=es_LA"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-link text-light" href="https://twitter.com/fedexteayuda"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-link text-light" href="https://www.linkedin.com/company/fedex?original_referer=https%3A%2F%2Fwww.google.com%2F"><i class="fab fa-linkedin-in"></i></a>
                    <a class="btn btn-link text-light" href="https://www.instagram.com/fedex/?hl=es"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
            <div class="col-lg-6 text-end">
                <div class="h-100 topbar-right d-inline-flex align-items-center text-white py-2 px-5">
                    <span class="fs-5 fw-bold me-2"><i class="fa fa-phone-alt me-2"></i>Comunicate:</span>
                    <span class="fs-5 fw-bold">0992722421/0987621067</span>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Navbar Start -->

    <nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top py-0 pe-5">
        <a href="index.html" class="navbar-brand ps-5 me-0">
            <h1 class="text-white m-0">FEDEX</h1>
        </a>
        <button type="button" class="navbar-toggler me-0" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto p-4 p-lg-0">
                <a href="<?php echo site_url("clientes/bienvenido"); ?>" class="nav-item nav-link active">BIENVENIDOS</a>
                <a href="<?php echo site_url("clientes/nosotros"); ?>" class="nav-item nav-link">NOSOTROS</a>
                <a href="<?php echo site_url("servicios/contactanos") ?>" class="nav-item nav-link">CONTACTANOS</a>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">SERVICIOS</a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="<?php echo site_url("servicios/aero"); ?>" class="dropdown-item">Transporte Aero</a>
                        <a href="<?php echo site_url("servicios/maritimo"); ?>" class="dropdown-item">Transporte Maritimo</a>
                        <a href="<?php echo site_url("servicios/complementarios"); ?>" class="dropdown-item">Complementarios</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">CLIENTES</a>
                    <div class="dropdown-menu bg-light m-0">
                        <a href="<?php echo site_url("clientes/nuevo"); ?>" class="dropdown-item">NUEVO</a>
                        <a href="<?php echo site_url("clientes/index"); ?>" class="dropdown-item">LISTA</a>
                        <a href="<?php echo site_url("reportes/clientes"); ?>" class="dropdown-item">Ubicación de clientes</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">PRODUCTOS</a>
                    <div class="dropdown-menu bg-light m-0">
                      <a href="<?php echo site_url("productos/nuevo"); ?>" class="dropdown-item">NUEVO</a>
                      <a href="<?php echo site_url("productos/index"); ?>" class="dropdown-item">LISTA</a>
                      </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">PEDIDOS</a>
                    <div class="dropdown-menu bg-light m-0">
                      <a href="<?php echo site_url("pedidos/nuevo"); ?>" class="dropdown-item">NUEVO</a>
                      <a href="<?php echo site_url("pedidos/index"); ?>" class="dropdown-item">LISTA</a>
                      <a href="<?php echo site_url("reportes/pedidos"); ?>" class="dropdown-item">Ubicación de los pedidos</a>
                      </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">SUCURSALES</a>
                    <div class="dropdown-menu bg-light m-0">
                      <a href="<?php echo site_url("sucursales/nuevo"); ?>" class="dropdown-item">NUEVO</a>
                      <a href="<?php echo site_url("sucursales/index"); ?>" class="dropdown-item">LISTA</a>
                      <a href="<?php echo site_url("reportes/sucursales"); ?>" class="dropdown-item">Ubicación de Sucursales</a>
                      </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">ENCOMIENDAS</a>
                    <div class="dropdown-menu bg-light m-0">
                      <a href="<?php echo site_url("encomiendas/nuevo"); ?>" class="dropdown-item">NUEVO</a>
                      <a href="<?php echo site_url("encomiendas/index"); ?>" class="dropdown-item">LISTA</a>
                      <a href="<?php echo site_url("reportes/encomiendas"); ?>" class="dropdown-item">Ubicación de Encomienda</a>
                      </div>
                </div>
                <!-- <a href="contact.html" class="nav-item nav-link">CONTACTANOS</a> -->

    </nav>
    <!-- Navbar End -->


    <!-- Carousel Start -->
    <!-- <div class="container-fluid px-0 mb-5">
        <div id="header-carousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="w-100" src="https://m.media-amazon.com/images/I/715JRiIEFSS.jpg" alt="Image">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-10 text-start">
                                    <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                        Trabajando contigo con experiencia</p>
                                    <h1 class="display-1 text-white mb-5 animated slideInRight">
                                        Tu Company Fedex</h1>
                                    <a href="" class="btn btn-primary py-3 px-5 animated slideInRight">Visitanos sera un gusto atenderle</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="w-100" src="https://newsismybusiness.com/wp-content/uploads/2022/12/FedEx-Peak-Season-1.jpg" alt="Image">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-10 text-start">
                                    <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                        Trabajando contigo con experiencia</p>
                                    <h1 class="display-1 text-white mb-5 animated slideInRight">
                                        Tu Company Fedex</h1>
                                    <a href="<?php echo site_url("clientes/nosotros"); ?>" class="btn btn-primary py-3 px-5 animated slideInRight">Visitamos</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="w-100" src="https://wtop.com/wp-content/uploads/2016/12/121516_fedex_2.jpg" alt="Image">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-10 text-start">
                                    <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                        Trabajando contigo con experiencia</p>
                                    <h1 class="display-1 text-white mb-5 animated slideInRight">
                                        Tu Company Fedex</h1>
                                    <a href="" class="btn btn-primary py-3 px-5 animated slideInRight">Visitamos</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#header-carousel" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    </div> -->
    <!-- Carousel End -->
