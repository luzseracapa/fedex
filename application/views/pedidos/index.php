<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">LISTA DE PEDIDO</p> </b></h1>
    </div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('pedidos/nuevo'); ?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Pedido
       </a>
    <br>
    <br>
    <br>
  <?php if ($listadoPedidos): ?>
  <table id="tabla-pedidos" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">FECHA DE PEDIDO</th>
      <th class="text-center">DESCRIPCION DEL PEDIDO</th>
      <th class="text-center">CANTIDAD</th>
      <th class="text-center">LATITUD</th>
      <th class="text-center">LOGITUD</th>
      <th class="text-center">LATITUD DE ENVIO</th>
      <th class="text-center">LOGITUD DE ENVIO</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoPedidos as $Temporal): ?>
      <tr>
        <td class="text-center"> <?php echo $Temporal->id_ped ?></td>
        <td class="text-center"> <?php echo $Temporal->fecha_ped ?></td>
        <td class="text-center"> <?php echo $Temporal->descripcion_ped?></td>
        <td class="text-center"> <?php echo $Temporal->cliente_id_clie ?></td>
        <td class="text-center"> <?php echo $Temporal->latitud_ped?></td>
        <td class="text-center"> <?php echo $Temporal->longitud_ped?></td>
        <td class="text-center"> <?php echo $Temporal->latitud_envio?></td>
        <td class="text-center"> <?php echo $Temporal->longitud_envio?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("pedidos/borrar");?>/<?php echo $Temporal->id_ped ; ?>"class="btn btn-danger"
            title="Eliminar Pedido" onclick="return confirm('Esta seguro de eliminar pedido');">
          <i class="glyphicon glyphicon-trash"></i>Eliminar</a>
          </button>
           <a href="<?php echo site_url("pedidos/nuevo") ?>" class="btn btn-danger"></i>Agregar</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existe pedidos</b></h3>
<?php endif; ?>

  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
