<br>
<?php if ($pedidos):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1>Listado de pedidos</h1>
            <tr>
                <th>ID</th>
                <th>FECHA DE PEDIDO</th>
                <th>DESCRIPCION DEL PEDIDO</th>
                <th>CANTIDAD</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>LONGITUD DE ENVIO</th>
                <th>LATITUD DE ENVIO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pedidos as $filaTemporal): ?>
            <tr>
                    <td>
                        <?php echo $filaTemporal->id_ped; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->fecha_ped; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->descripcion_ped; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->cliente_id_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->latitud_ped;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->longitud_ped; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->latitud_envio;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->longitud_envio; ?>
                    </td>
            </tr>
            <?php endforeach; ?>

        </tbody>

    </table>

    <?php else: ?>
        <h1>no hay pedidos</h1>
        <?php endif;?>
