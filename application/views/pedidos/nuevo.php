<div class="row">
    <div class="col-md-12 text-center">
        <h1><b><p style="color:#001f36;">Ingrese nuevo pedido </p> </b></h1>
    </div>
</div>
<br>
<br>
<form class="" id="frm_nuevo_pedido" action="<?php echo site_url("pedidos/guardarPedidos"); ?>" method="post">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <label for="">Ingrese la fecha del pedido
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="date" placeholder="Ingrese DD/MM/AA" class="form-control" name="fecha_ped" value="" id="fecha_ped" required>
                <br>
            </div>
            <div class="col-md-4">
                <label for="">Ingrese nombre pedido
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="text" placeholder="Ingrese nombre pedido" class="form-control" name="nombre_ped" value="" id="nombre_ped" required>
                <br>
            </div>
            <div class="col-md-4">
                <label for="">Ingrese la descripcion del pedido
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="text" placeholder="Ingrese la descripcion del pedido" class="form-control" name="descripcion_ped" value="" id="descripcion_ped" required>
                <br>
            </div>
          </div>
            <div class="row">
               <div class="col-md-6">
                 <label for="">Ingrese el numero de producto
                    <span class="obligatorio">(Obligatorio)</span>
                 </label>
                <br>
                 <input type="number" placeholder="Ingrese el numero de producto" class="form-control" name="cliente_id_clie" value="" id="cliente_id_clie" required>
                <br>
              </div>
            <div class="col-md-6">
                <label for="">Ingrese peso del pedido
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="number" placeholder="Ingrese peso" class="form-control" name="peso_ped" value="" id="peso_ped" required>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Ingrese su ubicacion actual</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <label for="">Ingrese su latitud
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="float" placeholder="Ingrese su latitud" class="form-control" name="latitud_ped" value="" id="latitud_ped" readonly>
                <br>
            </div>
            <div class="col-md-3">
                <label for="">Ingrese su longitud
                    <span class="obligatorio">(Obligatorio)</span>
                </label>
                <br>
                <input type="float" placeholder="Ingrese su longitud" class="form-control" name="longitud_ped" value="" id="longitud_ped" readonly>
                <br>

            </div>
            <div class="col-md-6">
                <div id="mapa" style="height:200px; width: 100%; border:2px solid black;"></div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <h3>Ingrese la ubicacion de envio</h3>
        </div>
    </div>
    <div class="container">
    <div class="row">
        <div class="col-md-3 text-center">
            <label for="">Ingrese su latitud del envio
            <span class="obligatorio">(Obligatorio)</span></label>
            <br>
            <input type="float" placeholder="Ingrese su latitud" class="form-control" name="latitud_envio" value="" id="latitud_envio" readonly>
            <br>
        </div>
        <div class="col-md-3 text-center">
            <label for="">Ingrese su longitud del envio
            <span class="obligatorio">(Obligatorio)</span></label>
            <br>
            <input type="float" placeholder="Ingrese su longitud" class="form-control" name="longitud_envio" value="" id="longitud_envio" readonly>
            <br>
        </div>
        <div class="col-md-6">
            <div id="mapa2" style="height:200px; width: 100%; border:2px solid black;"></div>
        </div>
    </div>
        </div>
        <script type="text/javascript">
        function initMap()
        {
          var centro1=new google.maps.LatLng(0.642666457056745, -78.67639361548689);
          var mapa1=new google.maps.Map(
            document.getElementById('mapa'),
            {
              center:centro1,
              zoom:7,
              mapTypeId:google.maps.MapTypeId.ROADMAP
            }
          );
          var marcador1=new google.maps.Marker({
            position:centro1,
            map:mapa1,
            title:"Seleccione la direccion",
            icon: "<?php echo base_url('assets/images/verde.png'); ?>",
            draggable:true
          });
          ///// valores
          google.maps.event.addListener(marcador1,'dragend',function(){
            // alert ("Se termino el Drag");
            document.getElementById('latitud_ped').value=
            this.getPosition().lat();
            document.getElementById('longitud_ped').value=
            this.getPosition().lng();
          });
         var centro2=new google.maps.LatLng(0.642666457056745, -78.67639361548689);
         var mapa2=new google.maps.Map(
           document.getElementById('mapa2'),
           {
             center:centro2,
             zoom:7,
             mapTypeId:google.maps.MapTypeId.ROADMAP
           }
         );
         var marcador2=new google.maps.Marker({
           position:centro2,
           map:mapa2,
           title:"Seleccione la direccion",
           icon: "<?php echo base_url('assets/images/verde.png'); ?>",
           draggable:true
         });
         ///// valores
         google.maps.event.addListener(marcador2,'dragend',function(){
           // alert ("Se termino el Drag");
           document.getElementById('latitud_envio').value=
           this.getPosition().lat();
           document.getElementById('longitud_envio').value=
           this.getPosition().lng();
         });
       }///cierre de la funcion
      </script>
    <br>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <br>
            <button type="submit" name="button" class="btn btn-primary">Guardar</button>
            <a href="<?php echo site_url(); ?>/pedidos/listas" class="btn btn-danger">Cancelar</a>
        </div>
    </div>
</form>
<script type="text/javascript">
    $("#frm_nuevo_pedido").validate({
        rules: {
          nombre_ped: {
              required: true,
              minlength: 3,
              maxlength: 250,
              letras: true
          },
            descripcion_ped: {
                required: true,
                minlength: 3,
                maxlength: 250,
                letras: true
            },
        },
        messages: {
          nombre_ped: {
              required: "Ingrese su nombre por favor",
              minlength: "Ingrese el nombre al menos de 3 digitos",
              maxlength: "Incorrecto",
              letras: "Este campo acepta solo letras"
          },
            descripcion_ped: {
                required: "Ingrese su descripcion por favor",
                minlength: "Ingrese el nombre al menos de 3 digitos",
                maxlength: "Incorrecto",
                letras: "Este campo acepta solo letras"
            },
        }
    });
</script>
<br><br>
