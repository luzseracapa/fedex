<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">LISTA DE PRODUCTOS</p> </b></h1>
    </div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('productos/nuevo'); ?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Producto
       </a>
<br>
<br>
<br>
<?php if ($listadoProductos): ?>
    <table id="tabla-productos" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th class="text-center">ID</th>
                <th class="text-center">NOMBRE</th>
                <th class="text-center">PRECIO</th>
                <th class="text-center">DESCRIPCION</th>
                <th class="text-center">PEDIDO</th>
                <th class="text-center">ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoProductos as $Temporal): ?>
                <tr>
                    <td class="text-center"><?php echo $Temporal->id_pro ?></td>
                    <td class="text-center"><?php echo $Temporal->nombre_pro ?></td>
                    <td class="text-center"><?php echo $Temporal->precio_pro ?></td>
                    <td class="text-center"><?php echo $Temporal->descripcion_pro ?></td>
                    <td class="text-center"><?php echo $Temporal->pedido_id_ped ?></td>
                    <td class="text-center">
                        <a href="<?php echo site_url("productos/agregar");?>" class="btn btn-danger">
                            <i class="glyphicon glyphicon-trash"></i>Agregar
                        </a>
                        <a href="<?php echo site_url("productos/borrar") ?>/<?php echo $Temporal->id_pro ; ?>" class="btn btn-danger"
                        title="Eliminar Producto"
                        onclick="return confirm('Esta seguro de eliminar producto');"></i>Eliminar</a>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <h3><b>No existe productos</b></h3>
<?php endif; ?>

<!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
</script> -->
<br>
<br>
<br>
