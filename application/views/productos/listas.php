<br>
<?php if ($productos): ?>
    <h1>Lista de productos</h1>
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>PRECIO</th>
                <th>DESCRIPCION</th>
                <th>PEDIDO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productos as $filaTemporal): ?>
                <tr>
                    <td><?php echo $filaTemporal->id_pro;?></td>
                    <td><?php echo $filaTemporal->nombre_pro;?></td>
                    <td><?php echo $filaTemporal->precio_pro;?></td>
                    <td><?php echo $filaTemporal->descripcion_pro;?></td>
                    <td>
                        <?php echo $filaTemporal->pedido_id_ped; ?>
                    </td>

                    <!-- para poner un lapz dentro de un forech para poder editar -->

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <h1>No hay productos</h1>
<?php endif; ?>
<br>
<br>
