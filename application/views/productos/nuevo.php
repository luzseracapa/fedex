<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Ingrese nuevo producto </p> </b></h1></div>
    </div>
</div>
<br>
<br>
<form class="" id="frm_nuevo_producto" action="<?php echo site_url("productos/guardarProductos"); ?>" method="post">
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <label for="">Ingrese Nombre
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="text" placeholder="Ingrese su nombre" class="form-control" name="nombre_pro" value=""
            id="nombre_pro" required>
            <br>
        </div>
        <div class="col-md-6">
            <label for="">Ingrese el precio
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="number" placeholder="Ingrese el precio" class="form-control" name="precio_pro" value=""
            id="precio_pro"required>
            <br>
        </div>
        </div>
        <div class="row">
          <div class="col-md-6">
              <label for="">Ingrese la descripcion
              <span class="obligatorio">(Obligatorio)</span>
            </label>
              <br>
              <input type="text" placeholder="Ingrese la descripcion" class="form-control" name="descripcion_pro" value=""
              id="descripcion_pro"required>
              <br>
          </div>
          <div class="col-md-6">
              <label for="">Ingrese el id pedido
              <span class="obligatorio">(Obligatorio)</span>
            </label>
              <br>
              <input type="number" placeholder="Ingrese su id pedido" class="form-control" name="pedido_id_ped" value=""
              id="pedido_id_ped"required>
              <br>
          </div>
        </div>
</div>
        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <br>
                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                <a href="<?php echo site_url(); ?>/productos/listas"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>

</form>
<br>
<script type="text/javascript">
  $("#frm_nuevo_producto").validate({
    rules:{
      nombre_pro:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      precio_pro:{
        required:true,
        minlength:1,
        maxlength:250,
        digits:true
      },

    },
    messages:{
      nombre_pro:{
        required:"Ingrese su nombre por favor",
        minlength:"Ingrese el nombre al menos de 3 digitos",
        maxlength:"Nombre incorrecto",
        letras:"Este campo acepta solo letras"
      },
      precio_pro:{
        required:"Ingrese su precio por favor",
        minlength:"Ingrese el precio al menos de 1",
        maxlength:"Precio incorrecto",
        digits:"Este campo acepta solo numeros"
      },


    }
  });
</script>
