<div class="row">
  <div class="col-md-12 text-center">
    <h1> <b> MAPA DE LA UBICACIÓN DE LOS CLIENTES</b> </h1>
  </div>
</div>
<div class="container">
			<div class="col-md-12">
				<div id="mapaClientes"
				style="height:500px; width:100%; border:2px solid black;"></div>
			</div>
		</div>
		<script type="text/javascript">
  		function initMap(){////cualquier nombre que nos guste en la funcion
  			var centro=///cualquier nombre
  			new google.maps.LatLng(-0.9322600236325245, -78.61529501495609); ///funcion de google maps para crear coordenadas "google.maps.LatLng"
  			///colocar cualquier nombre
  			//google.maps.Map nos permite construir el mapa
  			var mapaClientes=new google.maps.Map(document.getElementById('mapaClientes'), //nombre del id de arriba
  			{
  				center:centro,
  				zoom: 8,
  				mapTypeId:google.maps.MapTypeId.HYBRID
  			}
  		);
  		<?php if($cliente): ?>
  		<?php foreach ($cliente as $lugarTemporal): ?>
  		var coordenadaTemporal=new google.maps.LatLng(<?php echo $lugarTemporal->latitud_clie; ?>,<?php echo $lugarTemporal->longitud_clie; ?>);///nombre en variable salgan muchos indicadores o marcdadores
  		var marcador=new google.maps.Marker({
  			position:coordenadaTemporal,
  			title:"<?php echo $lugarTemporal->nombre_clie; ?>",
        icon: "<?php echo base_url('assets/images/icono.png'); ?>",

  			map:mapaClientes ///el nombre de var
  		});
  		<?php endforeach; ?>
  		<?php endif ?>
	}//cierre de la funcion init map
		</script>
