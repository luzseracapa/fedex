<div class="row">
  <div class="col-md-12 text-center">
    <h1><b>MAPA DE LA UBICACIÓN DE ENCOMIENDAS</b></h1>
  </div>
</div>
<div class="container">
  <div class="col-md-12">
    <div id="mapaEncomiendas" style="height: 500px; width: 100%; border: 2px solid black;"></div>
  </div>
</div>

<script type="text/javascript">
  function initMap() {
    var centro = new google.maps.LatLng(-0.9322600236325245, -78.61529501495609);

    var mapaEncomiendas = new google.maps.Map(document.getElementById('mapaEncomiendas'), {
      center: centro,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.HYBRID
    });

    <?php if($encomienda): ?>
    <?php foreach ($encomienda as $lugarTemporal): ?>
    var coordenadaTemporal = new google.maps.LatLng(<?php echo $lugarTemporal->latitud_clie_enco; ?>, <?php echo $lugarTemporal->longitud_clie_enco; ?>);
    var marcador = new google.maps.Marker({
      position: coordenadaTemporal,
      title: "<?php echo $lugarTemporal->nombre_clie_enco; ?>",
      icon: "<?php echo base_url('assets/images/icono.png'); ?>",
      map: mapaEncomiendas
    });
    <?php endforeach; ?>
    <?php endif; ?>
  }

  // Llama a la función initMap cuando se carga el script de Google Maps
  function loadScript() {
    var script = document.createElement('script');
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAZ9im5myj2rT_iJPbbftxHmOdJWM9PV3g&libraries=places&callback=initMap';
    document.body.appendChild(script);
  }

  // Carga el script de Google Maps cuando se carga la página
  window.onload = loadScript;
</script>
