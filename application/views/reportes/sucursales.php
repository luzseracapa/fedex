<div class="row">
  <div class="col-md-12 text-center">
    <h1><b>MAPA DE LA UBICACIÓN DE LAS SUCURSALES</b></h1>
  </div>
</div>
<div class="container">
  <div class="col-md-12">
    <div id="mapaSucursales" style="height: 500px; width: 100%; border: 2px solid black;"></div>
  </div>
</div>
<script type="text/javascript">
  function initMap() {
    var centro = new google.maps.LatLng(-0.9322600236325245, -78.61529501495609);
    var mapaSucursales = new google.maps.Map(document.getElementById('mapaSucursales'), {
      center: centro,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.HYBRID
    });

    <?php if ($sucursal): ?>
    <?php foreach ($sucursal as $lugarTemporal): ?>
    var coordenadaTemporal = new google.maps.LatLng(<?php echo $lugarTemporal->latitud_suc; ?>, <?php echo $lugarTemporal->longitud_suc; ?>);
    var marcador = new google.maps.Marker({
      position: coordenadaTemporal,
      title: "<?php echo $lugarTemporal->nombre_suc; ?>",
      icon: "<?php echo base_url('assets/images/icono.png'); ?>",
      map: mapaSucursales
    });
    <?php endforeach; ?>
    <?php endif; ?>
  }

  // Lógica adicional o llamadas a funciones, etc.

  // Llamada a la función initMap para inicializar el mapa
  initMap();
</script>
