<br>
<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Principales mercados del mundo</p> </b></h1></div>
  </div>
  <div class="container">
    <div class="col-md-12 text-center">
      <p>Independientemente de que lo que necesite sea flexibilidad, reducir los tiempos de tránsito
        o acelerar la comercialización de sus productos, puede contar con nuestra experiencia de envíos
        tanto locales como internacionales, así como con la amplia gama de soluciones para gestionar todas
         sus importaciones y exportaciones aéreas. Mantenemos su mercancía en circulacion para mantener el
         ritmo de su cadena de suministro. </p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 text-center">
      <h3><b>NUESTRAS OFERTAS</b></h3>
  <li>Soluciones personalizadas a precios competitivos.</li>
  <li>Servicios de entrada y salida en la mayoría de los aeropuertos de todo el mundo.</li>
  <li>Cobertura completa y soluciones para cadenas de suministro globales.</li>
  <li>Paletización 32:32 del comprador y servicios directos al consignatario.</li>
  <li>Distribución local, incluida la recogida en origen y la entrega en destino.</li>
    </div>
    <div class="col-md-4 text-center">
      <img src="<?php echo base_url('assets/images/aer.jpg') ?>" alt="" width="200" height="200">
    </div>
  </div>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-md-6 text-center">
        <h3><b>NUESTRAS VENTAJAS</b></h3>
        <li>Entradas regionales para flexibilizar los costes y el tránsito.</li>
        <li>Servicio de atención al cliente especializado y experimentado con un único punto de contacto.</li>
        <li>Asistencia en los Despachos de Aduana y herramientas de seguimiento en línea.</li>
        <li>Opciones de precios de aeropuerto a aeropuerto, de aeropuerto a puerta, de puerta a aeropuerto y de puerta a puerta.</li>
    </div>
    <div class="col-md-4 text-center">
      <img src="<?php echo base_url('assets/images/Plane.jpg') ?>" alt="" width="200" height="200">
    </div>
  </div>
