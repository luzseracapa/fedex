<br>
<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Servicios complementarios especializados y personalizados</p> </b></h1></div>
  </div>
  <div class="container">
    <div class="col-md-12 text-center">
      <p>Podemos ofrecerle ayuda en los despachos de aduanas, soluciones de almacenamiento,
        opciones de recogida y entrega, seguro de mercancías y mucho más. Además, podrá combinar
        los servicios y las funciones que mejor se adapten a sus necesidades. </p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 text-center">
      <h3><b>NUESTRAS OFERTAS</b></h3>
      <p class="text-justify">
        *Almacenes <br>
        *Seguro de mercancías
        *Múltiples modos de transporte, recogida y distribución <br>
        *Informe, visibilidad y seguimiento del estado del envío en línea <br>
        *Ayuda en el control aduanero <br>
        *Servicios de consultoría comercial</p>
    </div>
    <div class="col-md-6 text-center">
      <img src="<?php echo base_url('assets/images/co1.jpg') ?>" alt="" width="200" height="200">
    </div>
  </div>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-md-6 text-center">
      <h3><b>NUESTRAS VENTAJAS</b></h3>
      <p class="text-justify">
        *Soluciones flexibles y completas para adaptarse a sus necesidades <br>
        *Servicio de atención especializado y experimentado con un único punto de contacto <br>
        *Elevados estándares de seguridad de envío desde la llegada hasta el destino</p>
    </div>
    <div class="col-md-6 text-center">
      <img src="<?php echo base_url('assets/images/co2.jpg') ?>" alt="" width="200" height="200">
    </div>
  </div>
