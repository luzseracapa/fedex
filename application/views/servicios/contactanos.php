<div class="container-fluid px-0 mb-5">
    <div id="header-carousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="w-100" src="https://m.media-amazon.com/images/I/715JRiIEFSS.jpg" alt="Image">
                <div class="carousel-caption">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 text-start">
                                <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                    Trabajando contigo con experiencia</p>
                                <h1 class="display-1 text-white mb-5 animated slideInRight">
                                    Tu Company Fedex</h1>
                                <a href="" class="btn btn-primary py-3 px-5 animated slideInRight">Visitanos sera un gusto atenderle</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="w-100" src="https://newsismybusiness.com/wp-content/uploads/2022/12/FedEx-Peak-Season-1.jpg" alt="Image">
                <div class="carousel-caption">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 text-start">
                                <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                    Trabajando contigo con experiencia</p>
                                <h1 class="display-1 text-white mb-5 animated slideInRight">
                                    Tu Company Fedex</h1>
                                <a href="<?php echo site_url("clientes/nosotros"); ?>" class="btn btn-primary py-3 px-5 animated slideInRight">Visitamos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="w-100" src="https://wtop.com/wp-content/uploads/2016/12/121516_fedex_2.jpg" alt="Image">
                <div class="carousel-caption">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-10 text-start">
                                <p class="fs-5 fw-medium text-primary text-uppercase animated slideInRight">
                                    Trabajando contigo con experiencia</p>
                                <h1 class="display-1 text-white mb-5 animated slideInRight">
                                    Tu Company Fedex</h1>
                                <a href="" class="btn btn-primary py-3 px-5 animated slideInRight">Visitamos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#header-carousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#header-carousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</div>
<div class="row">
  <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Comuniquese con nosotros</p> </b></h1></div>
  </div>
  <div class="row">
    <div class="col-md-6 text-center">
      <img src="<?php echo base_url('assets/images/Call.png') ?>" alt="" width="150" height="150">
      <a href="tel:0992722421" data-tel="tel:0992722421" aria-label="Llame al 099 2272 2421" title="Llame al 099 2272 2421" target="_self" class=" fxg-link js-fxgc-init  fxg-link--blue fxg-link--align-center" data-analytics="link|Llame al 1 800 463 3339" onclick="FDX.DTM.pushLinkInfo(this)" style="color:inherit;">
        <b>Llame al 099 2272 2421</b>
        <span class="lock-hide"></span>
      </a>
    </div>
    <div class="col-md-6 text-center">
      <img src="<?php echo base_url('assets/images/add.png') ?>" alt="" width="150" height="150">
      <a href="http://localhost/fedex/index.php/clientes/bienvenido" aria-label="Información de contacto adicional" title="Información de contacto adicional" target="_self" class=" fxg-link js-fxgc-init  fxg-link--blue fxg-link--align-center" data-analytics="link|Información de contacto adicional" onclick="FDX.DTM.pushLinkInfo(this)" style="color:inherit;">
        <b>Información de contacto adicional</b>
        <span class="lock-hide"></span>
      </a>
    </div>
  <div class="row">
    <div class="col-md-12 text-center">
      <h3>Guia a servicos y conocemos de Fedex</h3>
      <p>Tu recurso para obtener información acerca de los servicios de FedEx</p>
      <a href="http://localhost/fedex/index.php/servicios/complementarios" target="_self" class="fxg-link fxg-link--rounded_button " data-analytics="foc|Explorar" onclick="FDX.DTM.pushLinkInfo(this)">
        Explorar
      </a>
    </div>
  </div>
