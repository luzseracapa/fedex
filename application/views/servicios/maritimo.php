<br>
<br>
<div class="row">
  <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Transporte de mercancías</p> </b></h1></div>
  </div>
  <div class="container">
    <div class="col-md-12 text-center">
      <p>Ofrece una experiencia internacional en el transporte de mercancías, así como
        soluciones de transporte integradas. Gracias a múltiples opciones de navegación
        y entrada, nuestros servicios por mar le ofrecen flexibilidad tanto en la velocidad como en los precios. </p>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 text-center">
      <h3><b>NUESTRAS OFERTAS</b></h3>
      <p class="text-justify">
        *Servicios personalizados para sus opciones de productos, horarios, recogida y entrega <br>
        *Carga de contenedor completo (FCL) y carga de contenedor de carga parcial (LCL) <br>
        *Servicio de LCL puerta a puerta con entrega de FedEx Freight en EE. UU. gracias a FedEx International Direct Priority Ocean (IDPO)</p>
    </div>
    <div class="col-md-6 text-center">
      <img src="<?php echo base_url('assets/images/m1.jpg') ?>" alt="" width="200" height="200">
    </div>
  </div>
  <br>
  <br>
  <br>
  <div class="row">
    <div class="col-md-6 text-center">
      <h3><b>NUESTRAS VENTAJAS</b></h3>
      <p class="text-justify">
        *Flexibilidad en la navegación, los tiempos de tránsito y las opciones de precios al conectarse con la red de FedEx <br>
        *Gestión del transporte con servicio completo puerta a puerta, puerto a puerto, puerto a puerta y puerta a puerta <br>
        *Servicios de consolidación del comprador para reducir las tasas.</p>
    </div>
    <div class="col-md-6 text-center">
      <img src="<?php echo base_url('assets/images/m2.jpg') ?>" alt="" width="200" height="200">
    </div>
  </div>
