<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Lista de Sucursales a nivel mundial </p> </b></h1></div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('sucursales/nuevo'); ?>" class="btn btn-primary">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Sucursal
       </a>
       <br>
       <br>
    </div>
    <br>
    <br>
    <br>
  <?php if ($listadoSucursales): ?>
  <table id="tabla-sucursales" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">NOMBRE DE LA SUCURSAL</th>
      <th class="text-center">TELEFONO</th>
      <th class="text-center">CORREO ELECTRONICO</th>
      <th class="text-center">LATITUD</th>
      <th class="text-center">LOGITUD</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoSucursales as $Temporal): ?>
      <tr>
        <td class="text-center"><?php echo  $Temporal->id_suc ?></td>
        <td class="text-center"> <?php echo $Temporal->nombre_suc ?></td>
        <td class="text-center"> <?php echo $Temporal->telefono_suc ?></td>
        <td class="text-center"> <?php echo $Temporal->email_suc ?></td>
        <td class="text-center"> <?php echo $Temporal->latitud_suc?></td>
        <td class="text-center"> <?php echo $Temporal->longitud_suc?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("sucursales/borrar");?>/<?php echo $Temporal->id_suc ; ?>"class="btn btn-danger"
            title="Eliminar Encomienda" onclick="return confirm('Esta seguro de eliminar encomienda');">
          <i class="glyphicon glyphicon-trash" ></i>Eliminar</a>
          </button>
           <a href="<?php echo site_url("sucursales/nuevo") ?>" class="btn btn-danger"></i>Agregar</a>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existen sucursales</b></h3>
<?php endif; ?>

  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
