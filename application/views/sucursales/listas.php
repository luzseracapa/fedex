<br>
<?php if ($sucursales):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1>Listado de sucursales a nivel mundial</h1>
            <tr>
                <th>ID</th>
                <th>NOMBRE SUCURSAL</th>
                <th>TELEFONO</th>
                <th>CORREO ELECTRONICO</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sucursales as $filaTemporal): ?>
            <tr>
                    <td>
                        <?php echo $filaTemporal->id_suc; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_suc; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->telefono_suc; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->email_suc; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->latitud_suc;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->longitud_suc; ?>
                    </td>
            </tr>
            <?php endforeach; ?>

        </tbody>

    </table>

    <?php else: ?>
        <h1>no hay sucursales</h1>
        <?php endif;?>
