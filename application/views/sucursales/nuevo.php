<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Ingrese nueva Sucursal </p> </b></h1></div>
    </div>
</div>
<br>
<br>
<form class="" id="frm_nuevo_sucursal" action="<?php echo site_url("sucursales/guardarSucursales"); ?>" method="post">
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <label for="">Ingrese Nombre de sucursal
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="text" placeholder="Ingrese el nombre de la sucursal" class="form-control" name="nombre_suc" value=""
            id="nombre_suc" required>
            <br>
        </div>
        <div class="col-md-4">
            <label for="">Ingrese el telefono
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="number" placeholder="Ingrese el numero de telefono" class="form-control" name="telefono_suc" value=""
            id="telefono_suc"required>
            <br>
        </div>
        <div class="col-md-4">
            <label for="">Ingrese su correo electrónico
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="text" placeholder="Ingrese su correo electronico" class="form-control" name="email_suc" value=""
            id="email_suc"required>
            <br>
        </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Ingrese su direccion</h3>
            </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <label for="">Ingrese su latitud
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="float" placeholder="Ingrese su latitud" class="form-control" name="latitud_suc" value=""
            id="latitud_suc"readonly>
            <br>
        </div>
        <div class="col-md-3">
            <label for="">Ingrese su longitud
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="float" placeholder="Ingrese su longitud" class="form-control" name="longitud_suc" value=""
            id="longitud_suc"readonly>
            <br>
        </div>
        <div class="col-md-6">
          <div id="mapaUbicacion" style="height:200px; width: 100%; border:2px solid black;"></div>
        </div>
      </div>
</div>
        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <br>
                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                <a href="<?php echo site_url(); ?>/sucursales/listas"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>
        <br>
        <br>
        <!-- //////MAPA PARA SELECCIONAR -->
    <!-- <div class="container">
      <div class="container">
        <div id="mapaUbicacion"
        style="height:400px; width: 100%;
        border:2px solid black;"></div>
      </div>
    </div> -->
    <script type="text/javascript">
    function initMap(){
      var centro=new google.maps.LatLng(-1.642666457056745, -78.67639361548689);
      var mapa1=new google.maps.Map(
        document.getElementById('mapaUbicacion'),
        {
          center:centro,
          zoom:7,
          mapTypeId:google.maps.MapTypeId.ROADMAP
        }
      );
      var marcador=new google.maps.Marker({
        position:centro,
        map:mapa1,
        title:"Seleccione la direccion",
        icon: "<?php echo base_url('assets/images/verde.png'); ?>",
        draggable:true
      });
      ///// valores
      google.maps.event.addListener(marcador,'dragend',function(){
        // alert ("Se termino el Drag");
        document.getElementById('latitud_suc').value=
        this.getPosition().lat();
        document.getElementById('longitud_suc').value=
        this.getPosition().lng();
      });
    }///cierre de la funcion
    </script>
    <br>
    <br>

</form>
<script type="text/javascript">
  $("#frm_nuevo_sucursal").validate({
    rules:{
      nombre_suc:{
        required:true,
        minlength:3,
        maxlength:250,
        letras:true
      },
      telefono_suc:{
        required:true,
        minlength:10,
        maxlength:10,
        digits:true
      },

    },
    messages:{
      nombre_suc:{
        required:"Ingrese su nombre por favor",
        minlength:"Ingrese el nombre al menos de 3 digitos",
        maxlength:"Nombre incorrecto",
        letras:"Este campo acepta solo letras"
      },
      telefono_suc:{
        required:"Ingrese su telefono por favor",
        minlength:"Ingrese el telefono al menos de 10",
        maxlength:"Incorrecto",
        digits:"Este campo acepta solo numeros"
      },


    }
  });
</script>

<br><br>
